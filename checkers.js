var PIECE_TYPE = {
    WHITE:  "white_piece.png",
    BLACK: "black_piece.png",
    WHITE_QUEEN: "white_queen.png",
    BLACK_QUEEN: "black_queen.png"
};
var PIECE_COLOR = {
    WHITE:  "white",
    BLACK: "black"
};
var FIELD_COLOR = {
    EVEN:  "#56ab64",
    ODD: "#f3f3e1",
    EVEN_HOVER:  "#8ed299",
    EVEN_SELECTED:  "#8bce96",
    EVEN_HIGHLIGHT:  "#92d59C",
};
var fieldSize = 80;


function AlphaBetaStaticOrder(depth, heuristicFunc){
    this.depth = depth;
    this.player = null;
    this.heuristicFunc = heuristicFunc;
    this.cuts = 0;
    this.findMove = function(checkerboard){
        this.cuts = 0;
        var pieces = checkerboard.whiteMoves ? checkerboard.whitePieces : checkerboard.blackPieces;
        var opponentPieces = !checkerboard.whiteMoves ? checkerboard.whitePieces : checkerboard.blackPieces;
        console.log(new Date());
        var result = this.alphaBeta(checkerboard, this.depth, Number.MIN_SAFE_INTEGER,Number.MAX_SAFE_INTEGER, true, pieces[0].color, heuristicFunc(pieces, opponentPieces, null));
        console.log((checkerboard.whiteMoves ? "WHITE : " : "BLACK: ") + result[0] + " CUTS: " + this.cuts);
        console.log(new Date());
        return [checkerboard.board[result[1][0][1]][result[1][0][0]], checkerboard.board[result[1][1][1]][result[1][1][0]]];
    }

    this.alphaBeta = function(checkerboard, depth, alpha, beta, maximizingPlayer, playerColor, startVal){
        var engine = checkerboard.gameEngine;
        var playerPieces = playerColor == PIECE_COLOR.WHITE ? checkerboard.whitePieces : checkerboard.blackPieces;
        var opponentPieces = playerColor == PIECE_COLOR.WHITE ? checkerboard.blackPieces : checkerboard.whitePieces;

        var isGameFinished = checkerboard.gameEngine.isGameFinished(checkerboard.whitePieces, checkerboard.blackPieces, checkerboard.queenMovesWithoutProgress, checkerboard.board, checkerboard.blackTop);
        if(isGameFinished && ((playerColor == PIECE_COLOR.WHITE && isGameFinished == 'WHITE WON') || (playerColor == PIECE_COLOR.BLACK && isGameFinished == 'BLACK WON'))) {
            return [500 * (depth + 1), null]
        } else  if(isGameFinished && ((playerColor == PIECE_COLOR.WHITE && isGameFinished == 'BLACK WON') || (playerColor == PIECE_COLOR.BLACK && isGameFinished == 'WHITE WON'))) {
            return [- 500 * (depth + 1), null]
        } else  if(isGameFinished && isGameFinished == 'DRAW') {
            return [0, null];
        }
        var pieces = checkerboard.whiteMoves ? checkerboard.whitePieces : checkerboard.blackPieces;
        var moves = []
        for(var i = 0; i < pieces.length; ++i){
            if(pieces[i].value > 0){
                moves.push([[pieces[i].x, pieces[i].y], engine.getLegalMoves(pieces[i], checkerboard.whitePieces, checkerboard.blackPieces, checkerboard.board, checkerboard.blackTop)])
            }
        }
        var checkboardsValues = [];
        for(var piece = 0; piece < moves.length; ++piece){
            for(var dest = 0; dest < moves[piece][1].length; ++dest){
                var newCheckerboard = checkerboard.clone();
                var playerPieces = playerColor == PIECE_COLOR.WHITE ? newCheckerboard.whitePieces : newCheckerboard.blackPieces;
                var opponentPieces = playerColor == PIECE_COLOR.WHITE ? newCheckerboard.blackPieces : newCheckerboard.whitePieces;

                newCheckerboard.drawCheckboardOnScreen = false;
                newCheckerboard.movePiece(newCheckerboard.board[moves[piece][0][1]][moves[piece][0][0]], newCheckerboard.board[moves[piece][1][dest][1]][moves[piece][1][dest][0]]);
                checkboardsValues.push([heuristicFunc(playerPieces, opponentPieces, startVal), newCheckerboard, [moves[piece][0], moves[piece][1][dest]]]);
            }
        }
        if(maximizingPlayer){

            checkboardsValues.sort(function(a, b){
                return b[0] - a[0]
            });
            var v = [Number.MIN_SAFE_INTEGER, null];
            for(var piece = 0; piece < checkboardsValues.length; ++piece){
                newCheckerboard = checkboardsValues[piece][1];
                new_v = this.alphaBeta(newCheckerboard, depth-1, alpha, beta, false, playerColor, startVal);
                new_v[1] = checkboardsValues[piece][2];
                v = v[0] > new_v[0] ? v : new_v;

                alpha = Math.max(alpha, v[0]); 
                if(beta <= alpha){
                    this.cuts++;
                    break;
                }
            }
            return v;
        } else {
            checkboardsValues.sort(function(a, b){
                return a[0] - b[0]
            });
            var v = [Number.MAX_SAFE_INTEGER,null];
            for(var piece = 0; piece < checkboardsValues.length; ++piece){
                newCheckerboard = checkboardsValues[piece][1];
                new_v = this.alphaBeta(newCheckerboard, depth-1, alpha, beta, true, playerColor, startVal);
                new_v[1] = checkboardsValues[piece][2];
                v = v[0] < new_v[0] ? v : new_v;

                beta = Math.min(beta, v[0]);

                if(beta <= alpha){
                    this.cuts++;
                    break;
                }
                
            }

            return v;
        }
    }
}


function AlphaBeta(depth, heuristicFunc){
    this.depth = depth;
    this.player = null;
    this.heuristicFunc = heuristicFunc;
    this.cuts = 0;
    this.findMove = function(checkerboard){
        this.cuts = 0;
        var pieces = checkerboard.whiteMoves ? checkerboard.whitePieces : checkerboard.blackPieces;
        var opponentPieces = !checkerboard.whiteMoves ? checkerboard.whitePieces : checkerboard.blackPieces;

        var result = this.alphaBeta(checkerboard, this.depth, Number.MIN_SAFE_INTEGER,Number.MAX_SAFE_INTEGER, true, pieces[0].color, heuristicFunc(pieces, opponentPieces, null));
        console.log((checkerboard.whiteMoves ? "WHITE : " : "BLACK: ") + result[0] + " CUTS: " + this.cuts);
        console.log(new Date());
        return [checkerboard.board[result[1][0][1]][result[1][0][0]], checkerboard.board[result[1][1][1]][result[1][1][0]]];
    }

    this.alphaBeta = function(checkerboard, depth, alpha, beta, maximizingPlayer, playerColor, startVal){
        var engine = checkerboard.gameEngine;
        var playerPieces = playerColor == PIECE_COLOR.WHITE ? checkerboard.whitePieces : checkerboard.blackPieces;
        var opponentPieces = playerColor == PIECE_COLOR.WHITE ? checkerboard.blackPieces : checkerboard.whitePieces;

        var isGameFinished = checkerboard.gameEngine.isGameFinished(checkerboard.whitePieces, checkerboard.blackPieces, checkerboard.queenMovesWithoutProgress, checkerboard.board, checkerboard.blackTop);
        if(isGameFinished && ((playerColor == PIECE_COLOR.WHITE && isGameFinished == 'WHITE WON') || (playerColor == PIECE_COLOR.BLACK && isGameFinished == 'BLACK WON'))) {
            return [500 * (depth + 1), null]
        } else  if(isGameFinished && ((playerColor == PIECE_COLOR.WHITE && isGameFinished == 'BLACK WON') || (playerColor == PIECE_COLOR.BLACK && isGameFinished == 'WHITE WON'))) {
            return [- 500 * (depth + 1), null]
        } else  if(isGameFinished && isGameFinished == 'DRAW') {
            return [0, null];
        }
        if(depth == 0){
            var value = this.heuristicFunc(playerPieces, opponentPieces, startVal);
            return [value, null];
        }

        var pieces = checkerboard.whiteMoves ? checkerboard.whitePieces : checkerboard.blackPieces;
        var moves = []
        for(var i = 0; i < pieces.length; ++i){
            if(pieces[i].value > 0){
                moves.push([[pieces[i].x, pieces[i].y], engine.getLegalMoves(pieces[i], checkerboard.whitePieces, checkerboard.blackPieces, checkerboard.board, checkerboard.blackTop)])
            }
        }
        if(maximizingPlayer){

            var v = [Number.MIN_SAFE_INTEGER, null];
            for(var piece = 0; piece < moves.length; ++piece){
                for(var dest = 0; dest < moves[piece][1].length; ++dest){
                    var newCheckerboard = checkerboard.clone();

                    newCheckerboard.drawCheckboardOnScreen = false;
                    
                    newCheckerboard.movePiece(newCheckerboard.board[moves[piece][0][1]][moves[piece][0][0]], newCheckerboard.board[moves[piece][1][dest][1]][moves[piece][1][dest][0]]);
                    new_v = this.alphaBeta(newCheckerboard, depth-1, alpha, beta, false, playerColor,startVal);
                    new_v[1] = [moves[piece][0], moves[piece][1][dest]];
                    v = v[0] > new_v[0] ? v : new_v;

                    alpha = Math.max(alpha, v[0]); 
                    if(beta <= alpha){
                        this.cuts++;
                        break;
                    }
                }
            }
            return v;
        } else {
            var v = [Number.MAX_SAFE_INTEGER,null];
            for(var piece = 0; piece < moves.length; ++piece){
                for(var dest = 0; dest < moves[piece][1].length; ++dest){
                    var newCheckerboard = checkerboard.clone();

                    newCheckerboard.drawCheckboardOnScreen = false;
                    
                    newCheckerboard.movePiece(newCheckerboard.board[moves[piece][0][1]][moves[piece][0][0]], newCheckerboard.board[moves[piece][1][dest][1]][moves[piece][1][dest][0]]);
                    new_v = this.alphaBeta(newCheckerboard, depth-1, alpha, beta, true, playerColor, startVal);
                    new_v[1] = [moves[piece][0], moves[piece][1][dest]];
                    v = v[0] < new_v[0] ? v : new_v;

                    beta = Math.min(beta, v[0]);

                    if(beta <= alpha){
                        this.cuts++;
                        break;
                    }
                }
            }

            return v;
        }
    }
}


function RandomMove(){
    this.findMove = function(checkerboard){
        var engine = checkerboard.gameEngine;
        var pieces = checkerboard.whiteMoves ? checkerboard.whitePieces : checkerboard.blackPieces;
        var moves = []
        for(var i = 0; i < pieces.length; ++i){
            if(pieces[i].value > 0){
                moves.push([checkerboard.board[pieces[i].y][pieces[i].x], engine.getLegalMoves(pieces[i], checkerboard.whitePieces, checkerboard.blackPieces, checkerboard.board, checkerboard.blackTop)])
            }
        }
        while(!(possibilities = moves[Math.floor(Math.random()*moves.length)])[1].length){

        }
        var dest = possibilities[1][Math.floor(Math.random()*possibilities[1].length)];
        return [possibilities[0], checkerboard.board[dest[1]][dest[0]]];            
    }
}

function HumanPlayer() {
    this.notify = function(checkboard){
    }
};
function AIPlayer(algorithm) {
    this.algorithm = algorithm;
    this.algorithm.player = this;
    this.notify = function(checkerboard){
        var move = this.algorithm.findMove(checkerboard);
        setTimeout(function(){checkerboard.movePiece(move[0], move[1]);},700);
    }
};

function Piece(color, type, positionX, positionY) {
	this.color = color;
    this.type = type;
    this.x = positionX;
    this.y = positionY;
    this.value = 1;

    this.clone = function() {
        var cloneObj = new Piece(this.color, this.type, this.x, this.y);
        cloneObj.value = this.value;
        return cloneObj;
    }
};

function Field(color, x, y) {
	this.x = x;
    this.y = y;
    this.color = color;
    this.originalColor = color;
    this.setPiece = function(piece) {
        this.piece = piece;
    }
    this.setDomElem = function(domElem){
        this.domElem = domElem;
    }
    this.resetColor = function(){
        this.color = this.originalColor;
    }
    this.coords = [this.x, this.y];

    this.clone = function(){
        var cloneObj = new Field(this.color, this.x, this.y);
        cloneObj.originalColor = this.originalColor;
        if(this.piece){
            cloneObj.setPiece(this.piece.clone());
        }
        cloneObj.setDomElem(this.domElem);
        return cloneObj;
    }
};

function CheckersRulesEngine() {
    this.getBoardturnFactor = function(piece, blackTop){
        var boardturn_factor = 1;
        if(piece.color == PIECE_COLOR.WHITE && blackTop || !piece.color == PIECE_COLOR.WHITE && !blackTop){
            boardturn_factor = -1;
        } 
        return boardturn_factor;
    }

    this.getPlayersPieces = function(piece, whitePieces,blackPieces){
        var playerPieces = blackPieces;
        if(piece.color == PIECE_COLOR.WHITE){
            playerPieces = whitePieces;
        }
        return playerPieces;
    }
    this.getLegalMoves = function(piece, whitePieces, blackPieces, board, blackTop) {
        var boardturn_factor = this.getBoardturnFactor(piece, blackTop);
        var playerPieces = this.getPlayersPieces(piece, whitePieces, blackPieces);
        var result = this.canJump(piece, board, blackTop)
        if(result.length > 0){
            return result;
        }
        if(this.canAnyJump(playerPieces, board, blackTop)){
            return result;
        }

        if(piece.value == 1){
            if(board[piece.y + boardturn_factor] && board[piece.y + boardturn_factor][piece.x+1] && !board[piece.y + boardturn_factor][piece.x+1].piece){
                result.push([piece.x+1, piece.y+boardturn_factor]);
            }

            if(board[piece.y + boardturn_factor] && board[piece.y + boardturn_factor][piece.x-1] &&!board[piece.y + boardturn_factor][piece.x-1].piece){
                result.push([piece.x-1, piece.y+boardturn_factor]);
            }
        } else if(piece.value > 1) {
            var aDir = bDir = cDir = dDir = true;
            for(var i = 1; i < board.length; ++i){
                if(aDir && board[piece.y + i * boardturn_factor] && board[piece.y + i * boardturn_factor][piece.x + i * 1] && !board[piece.y + i * boardturn_factor][piece.x + i * 1].piece){
                    result.push([piece.x + i * 1, piece.y + i * boardturn_factor]);
                } else {
                    aDir = false;
                }

                if(bDir && board[piece.y + i * boardturn_factor] && board[piece.y + i * boardturn_factor][piece.x - i * 1] &&!board[piece.y + i * boardturn_factor][piece.x - i * 1].piece){
                    result.push([piece.x - i * 1, piece.y + i * boardturn_factor]);
                } else {
                    bDir = false;
                }

                if(cDir && board[piece.y - i * boardturn_factor] && board[piece.y - i * boardturn_factor][piece.x + i * 1] && !board[piece.y - i * boardturn_factor][piece.x + i * 1].piece){
                    result.push([piece.x + i * 1, piece.y - i * boardturn_factor]);
                } else {
                    cDir = false;
                }

                if(dDir && board[piece.y - i * boardturn_factor] && board[piece.y - i * boardturn_factor][piece.x - i * 1] &&! board[piece.y - i * boardturn_factor][piece.x - i * 1].piece){
                    result.push([piece.x - i * 1, piece.y - i * boardturn_factor]);
                }  else {
                    dDir = false;
                }
            }
        }
        return result;
    }

    this.canAnyJump = function(pieces, board, blackTop){
        for(var i = 0; i < pieces.length; ++i){
            if(this.canJump(pieces[i], board, blackTop).length > 0) {
                return true;
            }
        }
        return false;
    }
    this.canJump = function(piece, board, blackTop){
        var boardturn_factor = this.getBoardturnFactor(piece,blackTop);
        var result = [];
        if(piece.value == 1){
            if(board[piece.y + boardturn_factor] && board[piece.y + 2 * boardturn_factor] && board[piece.y + boardturn_factor][piece.x + 1] && board[piece.y + 2 * boardturn_factor][piece.x+2] && board[piece.y + boardturn_factor][piece.x+1].piece && board[piece.y + boardturn_factor][piece.x+1].piece.color != piece.color && !board[piece.y + 2*boardturn_factor][piece.x+2].piece){
                result.push([piece.x + 2, piece.y + 2 * boardturn_factor]);
            }
            if(board[piece.y + boardturn_factor] && board[piece.y + 2 * boardturn_factor] && board[piece.y + boardturn_factor][piece.x - 1] && board[piece.y + 2 * boardturn_factor][piece.x-2] && board[piece.y + boardturn_factor][piece.x-1].piece && board[piece.y + boardturn_factor][piece.x-1].piece.color != piece.color && !board[piece.y + 2 * boardturn_factor][piece.x-2].piece){
                result.push([piece.x - 2, piece.y + 2 * boardturn_factor]);
            }
            if(board[piece.y - boardturn_factor] && board[piece.y - 2 * boardturn_factor] && board[piece.y - boardturn_factor][piece.x + 1] && board[piece.y - 2 * boardturn_factor][piece.x+2] && board[piece.y - boardturn_factor][piece.x+1].piece && board[piece.y - boardturn_factor][piece.x+1].piece.color != piece.color && !board[piece.y - 2*boardturn_factor][piece.x+2].piece){
                result.push([piece.x + 2, piece.y - 2 * boardturn_factor]);
            }
            if(board[piece.y - boardturn_factor] && board[piece.y - 2 * boardturn_factor] && board[piece.y - boardturn_factor][piece.x - 1] && board[piece.y - 2 * boardturn_factor][piece.x-2] && board[piece.y - boardturn_factor][piece.x-1].piece && board[piece.y - boardturn_factor][piece.x-1].piece.color != piece.color && !board[piece.y - 2 * boardturn_factor][piece.x-2].piece){
                result.push([piece.x - 2, piece.y - 2 * boardturn_factor]);
            }
        } else if(piece.value > 1){
            var aDir = bDir = cDir = dDir = 0;
            for(var i = 1; i < board.length; ++i){
                if(aDir == 1 && board[piece.y + i * boardturn_factor] && board[piece.y + i * boardturn_factor][piece.x + i * 1] && !board[piece.y + i * boardturn_factor][piece.x + i * 1].piece){
                    result.push([piece.x + i * 1, piece.y + i * boardturn_factor]);
                } else if(board[piece.y + i * boardturn_factor] && board[piece.y + i * boardturn_factor][piece.x + i * 1] && board[piece.y + i * boardturn_factor][piece.x + i * 1].piece && board[piece.y + i * boardturn_factor][piece.x + i * 1].piece.color != piece.color){
                    ++aDir;
                } else if(board[piece.y + i * boardturn_factor] && board[piece.y + i * boardturn_factor][piece.x + i * 1] && board[piece.y + i * boardturn_factor][piece.x + i * 1].piece && board[piece.y + i * boardturn_factor][piece.x + i * 1].piece.color == piece.color){
                    aDir = 5;
                }
                
                if(bDir == 1 && board[piece.y + i * boardturn_factor] && board[piece.y + i * boardturn_factor][piece.x - i * 1] && !board[piece.y + i * boardturn_factor][piece.x - i * 1].piece){
                    result.push([piece.x - i * 1, piece.y + i * boardturn_factor]);
                } else if(board[piece.y + i * boardturn_factor] && board[piece.y + i * boardturn_factor][piece.x - i * 1] && board[piece.y + i * boardturn_factor][piece.x - i * 1].piece && board[piece.y + i * boardturn_factor][piece.x - i * 1].piece.color != piece.color){
                    ++bDir;
                } else if(board[piece.y + i * boardturn_factor] && board[piece.y + i * boardturn_factor][piece.x - i * 1] && board[piece.y + i * boardturn_factor][piece.x - i * 1].piece && board[piece.y + i * boardturn_factor][piece.x - i * 1].piece.color == piece.color){
                    bDir = 5;
                }

                if(cDir == 1 && board[piece.y - i * boardturn_factor] && board[piece.y - i * boardturn_factor][piece.x + i * 1] && !board[piece.y - i * boardturn_factor][piece.x + i * 1].piece){
                    result.push([piece.x + i * 1, piece.y - i * boardturn_factor]);
                } else if( board[piece.y - i * boardturn_factor] && board[piece.y - i * boardturn_factor][piece.x + i * 1] && board[piece.y - i * boardturn_factor][piece.x + i * 1].piece &&board[piece.y - i * boardturn_factor][piece.x + i * 1].piece.color != piece.color){
                    ++cDir;
                } else if( board[piece.y - i * boardturn_factor] && board[piece.y - i * boardturn_factor][piece.x + i * 1] && board[piece.y - i * boardturn_factor][piece.x + i * 1].piece &&board[piece.y - i * boardturn_factor][piece.x + i * 1].piece.color == piece.color){
                    cDir = 5;
                }

                if(dDir == 1 && board[piece.y - i * boardturn_factor] && board[piece.y - i * boardturn_factor][piece.x - i * 1] && !board[piece.y - i * boardturn_factor][piece.x - i * 1].piece){
                    result.push([piece.x - i * 1, piece.y - i * boardturn_factor]);
                } else if( board[piece.y - i * boardturn_factor] && board[piece.y - i * boardturn_factor][piece.x - i * 1] && board[piece.y - i * boardturn_factor][piece.x - i * 1].piece &&board[piece.y - i * boardturn_factor][piece.x - i * 1].piece.color != piece.color){
                    ++dDir;
                } else if(board[piece.y - i * boardturn_factor] && board[piece.y - i * boardturn_factor][piece.x - i * 1] && board[piece.y - i * boardturn_factor][piece.x - i * 1].piece &&board[piece.y - i * boardturn_factor][piece.x - i * 1].piece.color == piece.color){
                    dDir = 5;
                }

            }
        }
        return result;
    }
    this.becomesQueen = function(piece, blackTop){
        if(piece.color == PIECE_COLOR.WHITE && blackTop && piece.y == 0){
            return true;
        }
        if(piece.color == PIECE_COLOR.WHITE && !blackTop && piece.y == 7){
            return true;
        }
        if(piece.color == PIECE_COLOR.BLACK && blackTop && piece.y == 7){
            return true;
        }
        if(piece.color == PIECE_COLOR.BLACK && !blackTop && piece.y == 0){
            return true;
        }
        return false;
    }
    this.isGameFinished = function(whitePieces, blackPieces, queenMovesWithoutProgress, board, blackTop){
        var canWhiteMove = false;
        for(var i = 0; i < whitePieces.length && !canWhiteMove; ++i){
            if(this.getLegalMoves(whitePieces[i], whitePieces, blackPieces, board, blackTop).length > 0){
                canWhiteMove = true;
            }
        }
        if(!canWhiteMove) {
            return "BLACK WON";
        }

        var canBlackMove = false;
        for(var i = 0; i < blackPieces.length && !canBlackMove; ++i){
            if(this.getLegalMoves(blackPieces[i], whitePieces, blackPieces, board, blackTop).length > 0){
                canBlackMove = true;
            }
        }
        if(!canBlackMove) {
            return "WHITE WON";
        }

        if(queenMovesWithoutProgress == 15){
            return "DRAW";
        }
        if(whitePieces.reduce(function(previousValue, currentValue, index, array){ return previousValue + currentValue.value; }, 0) == 0){
            return "BLACK WON";
        }

        if(blackPieces.reduce(function(previousValue, currentValue, index, array){ return previousValue + currentValue.value; }, 0) == 0){
            return "WHITE WON";
        }
    }
};

function Checkerboard(size, canvas, whiteFirst, blackTop, engine, whitePlayer, blackPlayer) {
    this.size = size;
    this.whiteMoves = whiteFirst;
    this.canvas = canvas;
    this.whitePieces = []
    this.blackPieces = []
    this.board = [];
    this.gameEngine = engine;
    this.drawCheckboardOnScreen = true;
    this.blackTop = blackTop;
    this.queenMovesWithoutProgress = 0;
    this.whitePlayer = whitePlayer;
    this.blackPlayer = blackPlayer;
    this.currentPlayer = whiteFirst ? whitePlayer : blackPlayer;

    this.clone = function(){
        var cloneObj = new Checkerboard(this.size, this.canvas, this.whiteMoves, this.blackTop, this.gameEngine, this.whitePlayer, this.blackPlayer);
        cloneObj.board = [];
        for(i=0; i < this.size; i++) {
            cloneObj.board.push([]);
            for(j=0; j < this.size; j++) {
                var clonedField = this.board[i][j].clone();
                cloneObj.board[i].push(clonedField);
                if(clonedField.piece && clonedField.piece.color == PIECE_COLOR.WHITE){
                    cloneObj.whitePieces.push(clonedField.piece);
                } else if(clonedField.piece && clonedField.piece.color == PIECE_COLOR.BLACK){
                    cloneObj.blackPieces.push(clonedField.piece);
                }
            }
        }   
        return cloneObj;
    }
    this.drawCheckboard = function () {
        if(this.drawCheckboardOnScreen){
            $(canvas).css({'max-width': this.size * fieldSize, 'margin': '10px auto'})
            for(i=0; i < this.size; i++) {
                for(j=0; j < this.size; j++) {
                    var newElem = $("<div />").data('y', i).data('x', j).css({'background-color' : this.board[i][j].color, 'width' : fieldSize, 'height' : fieldSize, 'float': 'left'});

                    if(this.board[i][j].piece){
                        $("<img src='"+this.board[i][j].piece.type + "' alt='"+this.board[i][j].piece.color+"' />").css({'width' : fieldSize * 0.7, 'height' : 0.7*fieldSize, 'margin': 0.15*fieldSize}).appendTo(newElem);
                        if(this.currentPlayer instanceof HumanPlayer && this.whiteMoves && this.board[i][j].piece.color == PIECE_COLOR.WHITE || this.currentPlayer instanceof HumanPlayer && !this.whiteMoves && this.board[i][j].piece.color == PIECE_COLOR.BLACK){
                            this.addPieceListeners(newElem);
                        }
                    }
                    this.board[i][j].setDomElem(newElem)
                    newElem.appendTo($(canvas));           
                }
            }
        }
    };

    this.redrawCheckboard = function() {
        if(this.drawCheckboardOnScreen){
            for(i=0; i < this.size; i++) {
                for(j=0; j < this.size; j++) {
                    var domElem = this.board[i][j].domElem;
                    this.removePieceListeners(this.board[i][j]);

                    domElem.find('img').remove();
                    if(this.board[i][j].piece){

                        $("<img src='"+this.board[i][j].piece.type + "' alt='"+this.board[i][j].piece.color+"' />").css({'width' : fieldSize * 0.7, 'height' : 0.7*fieldSize, 'margin': 0.15*fieldSize}).appendTo(domElem);
                        if(this.currentPlayer instanceof HumanPlayer && this.whiteMoves && this.board[i][j].piece.color == PIECE_COLOR.WHITE || this.currentPlayer instanceof HumanPlayer &&  !this.whiteMoves && this.board[i][j].piece.color == PIECE_COLOR.BLACK){
                            this.addPieceListeners(domElem);
                        }
                    }
                }
            }
        }
    };
    this.removePieceListeners = function(field){
        var domElem = field.domElem;
        domElem.data('selected', false);
        domElem.data('highlight', false).css({'background-color': field.originalColor});
        domElem.off('mouseenter mouseleave');
        domElem.off('click');

    }
    this.addPieceListeners = function(domElem){
        if(this.drawCheckboardOnScreen){

            domElem.hover(function(e){
                if(!$(this).data('selected')){
                    $(this).css("background-color", FIELD_COLOR.EVEN_HOVER);
                }
            }, function(e){
                if(!$(this).data('selected')){
                    $(this).css("background-color", FIELD_COLOR.EVEN);
                }
            });
            thisRenamed = this;
            domElem.click(function(e){
                var $this = $(this);
                var wasSelected = $this.data('selected');
            // remove all previous selections
            for(i=0; i < thisRenamed.size; i++) {
                for(j=0; j < thisRenamed.size; j++) {
                    if(!thisRenamed.board[i][j].piece) {
                        thisRenamed.board[i][j].domElem.off('click');
                    }
                    thisRenamed.board[i][j].domElem.data('selected', false);
                    thisRenamed.board[i][j].domElem.data('highlight', false).css({'background-color': thisRenamed.board[i][j].originalColor});

                }
            }
            if(!wasSelected){
                $this.data('selected', true).css("background-color", FIELD_COLOR.EVEN_SELECTED);
                
                var sourceField = thisRenamed.board[$this.data('y')][$this.data('x')];
                var legalMoves = thisRenamed.gameEngine.getLegalMoves(sourceField.piece, thisRenamed.whitePieces, thisRenamed.blackPieces, thisRenamed.board, thisRenamed.blackTop);
                for(var i = 0; i < legalMoves.length; ++i){
                    var y = legalMoves[i][0];
                    var x = legalMoves[i][1];
                    thisRenamed.board[x][y].domElem.data('highlight', true).css("background-color", FIELD_COLOR.EVEN_HOVER).click(function(e){
                        $this = $(this);
                        thisRenamed.movePiece(sourceField, thisRenamed.board[$this.data('y')][$this.data('x')]);
                    });
                }
            } else {
                $this.data('selected', false).css("background-color", FIELD_COLOR.EVEN_HOVER);
            }
        });
        }
    };

    this.movePiece = function(source, destination){
        var piece = source.piece;
        var wasJumping = this.gameEngine.canJump(piece, this.board, this.blackTop).length != 0;
        piece.x = destination.x;
        piece.y = destination.y;
        destination.setPiece(piece);
        source.setPiece(null);
        this.handleJumping(piece, source, destination);

        if(this.gameEngine.becomesQueen(piece, this.blackTop)){
            piece.value = 2;
            piece.type = PIECE_TYPE.BLACK_QUEEN;
            if(piece.color == PIECE_COLOR.WHITE){
                piece.type = PIECE_TYPE.WHITE_QUEEN;
            }
            wasJumping = false;
        }
        if(wasJumping){
            if(this.gameEngine.canJump(piece, this.board, this.blackTop).length == 0){ 
                this.nextPlayer()
                destination.resetColor();
            }
        } else {
            if(piece.value > 1){
                ++this.queenMovesWithoutProgress;
            }
            else {
                this.queenMovesWithoutProgress = 0;
            }
            this.nextPlayer();
            destination.resetColor();
        }

        this.redrawCheckboard();
        var gameResult = this.gameEngine.isGameFinished(this.whitePieces, this.blackPieces, this.queenMovesWithoutProgress ,this.board, this.blackTop);

        if(gameResult && this.drawCheckboardOnScreen) {
            setTimeout(function(){alert(gameResult); } , 300);
        } else if(gameResult){
            return;
        } else if(this.drawCheckboardOnScreen){
            var thisRen = this;
            setTimeout(function(){ thisRen.currentPlayer.notify(thisRen); } , 300);

        }
    }

    this.handleJumping = function(piece, source, destination){
        if(this.gameEngine.canJump(piece, this.board, this.blackTop).length != 0){
            var xDiff = destination.x - source.x;
            var yDiff = destination.y - source.y;
            if(xDiff < 0){
                if(yDiff < 0) {
                    for(var x = source.x, y = source.y; x > destination.x && y > destination.y; x--, y--){
                        if(this.board[y][x].piece && this.board[y][x].piece != piece) {
                            this.board[y][x].piece.x = NaN;
                            this.board[y][x].piece.y = NaN;
                            this.board[y][x].piece.value = 0;
                            this.board[y][x].setPiece(null);

                        }   
                    }
                } else{                    
                    for(var x = source.x, y = source.y; x > destination.x && y < destination.y; x--, y++){
                        if(this.board[y][x].piece && this.board[y][x].piece != piece) {
                            this.board[y][x].piece.x = NaN;
                            this.board[y][x].piece.y = NaN;
                            this.board[y][x].piece.value = 0;
                            this.board[y][x].setPiece(null);
                        }                          

                    }
                }
            } else {
                if(yDiff < 0) {                   
                    for(var x = source.x, y = source.y; x < destination.x && y > destination.y; x++, y--){
                        if(this.board[y][x].piece && this.board[y][x].piece != piece) {
                            this.board[y][x].piece.x = NaN;
                            this.board[y][x].piece.y = NaN;
                            this.board[y][x].piece.value = 0;
                            this.board[y][x].setPiece(null);

                        }   
                    }
                } else {                  
                    for(var x = source.x, y = source.y; x < destination.x && y < destination.y; x++, y++){
                        if(this.board[y][x].piece && this.board[y][x].piece != piece) {
                            this.board[y][x].piece.x = NaN;
                            this.board[y][x].piece.y = NaN;
                            this.board[y][x].piece.value = 0;
                            this.board[y][x].setPiece(null);

                        }   
                    }
                }           
            }
            if(piece.value > 1){
                this.queenMovesWithoutProgress = 0;
            }
            return true;
        }
        return false;
    }
    this.nextPlayer = function() {
        this.whiteMoves = !this.whiteMoves;
        if(this.whiteMoves) {
            this.currentPlayer = whitePlayer;
        } else { 
            this.currentPlayer = blackPlayer;
        }
    }

    this.initGame = function() {
        var w = this.canvas.width / this.size;

        for(var i = 0; i < this.size; ++i) {
            this.board.push([]);
            y = i * w;
            for(var j = 0; j < this.size; ++j) {
                x = j * w;
                var color = i % 2 != j % 2 ? FIELD_COLOR.EVEN : FIELD_COLOR.ODD; 
                this.board[i].push(new Field(color, j, i));
                if(color == FIELD_COLOR.EVEN && (i < 3 || size - i <= 3)) {
                    var piece = new Piece("", "", j, i)

                    if(this.blackTop && i <= 3 || !this.blackTop && size - i <= 3) {
                        piece.color = PIECE_COLOR.BLACK;
                        piece.type = PIECE_TYPE.BLACK;
                        this.blackPieces.push(piece)
                    } else {
                        piece.color = PIECE_COLOR.WHITE;
                        piece.type = PIECE_TYPE.WHITE;

                        this.whitePieces.push(piece)
                    }

                    this.board[i][j].setPiece(piece)
                }
            }
        }
    }
}

function sumValues(playerPieces, opponentPieces){
    return playerPieces.reduce(function(previousValue, currentValue, index, array){ return previousValue + 5*currentValue.value * 2; }, opponentPieces.reduce(function(previousValue, currentValue, index, array){ return previousValue - 5 * currentValue.value; }, 0))
}

function sumOpponentValues(playerPieces, opponentPieces){
    return opponentPieces.reduce(function(previousValue, currentValue, index, array){ return previousValue - 5 * currentValue.value; }, 0);
}

function sumPlayerValues(playerPieces, opponentPieces){
    return playerPieces.reduce(function(previousValue, currentValue, index, array){ return previousValue + 5*currentValue.value; }, 0);
}

function sumValuesBonus(playerPieces, opponentPieces, startVal){
    var val = playerPieces.reduce(function(previousValue, currentValue, index, array){ return previousValue + 5*currentValue.value * 2; }, opponentPieces.reduce(function(previousValue, currentValue, index, array){ return previousValue - 5 * currentValue.value; }, 0))

    if(startVal){
        val = val + val - startVal;
    }
    return val;
}


function uberFunction(playerPieces, opponentPieces, startVal){
    var result = opponentPieces.reduce(function(previousValue, currentValue, index, array){ return previousValue - 5 * currentValue.value; }, 0);
    result += playerPieces.reduce(function(previousValue, currentValue, index, array){ return previousValue + 5*currentValue.value; }, 0);
    result *= Math.pow(10, 8);

    var val = playerPieces.reduce(function(previousValue, currentValue, index, array){
        if(currentValue.color == PIECE_COLOR.WHITE && currentValue.value == 1){
            return 7 - currentValue.y; 
        } else if(currentValue.value == 1) {
            return currentValue.y; 
        } 
    }, opponentPieces.reduce(function(previousValue, currentValue, index, array){ 
        if(currentValue.color == PIECE_COLOR.WHITE && currentValue.value == 1){
            return -7 + currentValue.y; 
        } else if (currentValue.value == 1) {
            return -currentValue.y; 
        }
    }, 0));
    val *= Math.pow(10, 6);
    result += val;

    return result;
}


function positionHeuristic(playerPieces, opponentPieces, startVal){
    var val = playerPieces.reduce(function(previousValue, currentValue, index, array){
        if(currentValue.color == PIECE_COLOR.WHITE ){
            return previousValue + 5*currentValue.value * 2 + 7 - currentValue.y; 
        } else {
            return previousValue + 5*currentValue.value * 2 + currentValue.y; 
        } 
    }, opponentPieces.reduce(function(previousValue, currentValue, index, array){ 
        if(currentValue.color == PIECE_COLOR.WHITE){
            return previousValue - 5 * currentValue.value - 7 + currentValue.y; 
        } else {
            return previousValue - 5 * currentValue.value - currentValue.y; 
        }
    }, 0));

    if(startVal){
        val = val - startVal;
    }
    return val;
}

var heuristics = [sumValues, sumOpponentValues, sumPlayerValues, sumValuesBonus, positionHeuristic];

function checkerGame(parentSelector, size){
	var can = $(parentSelector);
    var gameEngine = new CheckersRulesEngine();
    if($("#player1_mode").val() == 'alphabeta_basic'){
        player1 = new AIPlayer(new AlphaBeta(parseInt($("#player1_depth").val()), heuristics[parseInt($("#player1_heuristic1").val())]));
    } else if($("#player1_mode").val() == 'random'){
        player1 = new AIPlayer(new RandomMove());
    } else if($("#player1_mode").val() == 'alphabeta_staticorder'){
        player1 = new AIPlayer(new AlphaBetaStaticOrder(parseInt($("#player1_depth").val()), heuristics[parseInt($("#player1_heuristic1").val())]));
    } else {
        player1 = new HumanPlayer();

    }

    if($("#player2_mode").val() == 'alphabeta_basic'){
        player2 = new AIPlayer(new AlphaBeta(parseInt($("#player2_depth").val()), heuristics[parseInt($("#player2_heuristic1").val())]));
    } else if($("#player2_mode").val() == 'random'){
        player2 = new AIPlayer(new RandomMove());
    } else if($("#player2_mode").val() == 'alphabeta_staticorder'){
        player2 = new AIPlayer(new AlphaBetaStaticOrder(parseInt($("#player2_depth").val()), heuristics[parseInt($("#player2_heuristic1").val())]));
    } else {
        player2 = new HumanPlayer();

    }
    var checkerboard = new Checkerboard(8, can, true, true, gameEngine, player2, player1);
    checkerboard.initGame();

    checkerboard.drawCheckboard();
    setTimeout(function(){checkerboard.currentPlayer.notify(checkerboard);},500);

}

$(function(){
    $("#new_game").click(function(e){
        $("#checkerboardDiv").empty();
        checkerGame("#checkerboardDiv");
    });
});